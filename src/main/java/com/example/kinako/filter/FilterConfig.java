package com.example.kinako.filter;

import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class FilterConfig {

	@Bean
	public FilterRegistrationBean<LoginFilter> FilterSetting() {
		FilterRegistrationBean<LoginFilter> bean = new FilterRegistrationBean<LoginFilter>(new LoginFilter());
		//ログインフィルターを適用するURLの指定
		bean.addUrlPatterns("/kinako", "/user/*", "/message/*");
		bean.setOrder(1);
		return bean;
	}

	@Bean
	public FilterRegistrationBean<AdminFilter> adminFilter() {
		FilterRegistrationBean<AdminFilter> bean = new FilterRegistrationBean<AdminFilter>(new AdminFilter());
		//管理者権限フィルターを適用するURLの指定
		bean.addUrlPatterns("/user/*");
		bean.setOrder(2);
		return bean;
	}

}
