package com.example.kinako.filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Component;

import com.example.kinako.entity.User;

@Component
public class LoginFilter implements Filter {

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
				FilterChain chain) throws IOException, ServletException {

		HttpServletRequest req = (HttpServletRequest)request;
		HttpServletResponse res =(HttpServletResponse)response;

		HttpSession session = req.getSession();
		User loginUser = (User) session.getAttribute("loginUser");
		List<String> errorMessages = new ArrayList<String>();

		//ログインしてない場合
		if (loginUser == null) {
			errorMessages.add("ログインしてください");
			session.setAttribute("errorMessages", errorMessages);
			res.sendRedirect("/login");
			return;
		}

		chain.doFilter(request, response);
		}

	@Override
	public void init(FilterConfig config) {
	}

	@Override
	public void destroy() {
	}

}