package com.example.kinako.controller;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.example.kinako.entity.Comment;
import com.example.kinako.entity.Message;
import com.example.kinako.entity.User;
import com.example.kinako.service.CommentService;
import com.example.kinako.service.MessageService;

@Controller
public class HomeController {
	@Autowired
	MessageService messageService;

	@Autowired
	CommentService commentService;

	@Autowired
	HttpSession session;

	//Top画面（and投稿一覧画面）-> 別Controllerを作成するかは要相談
	@GetMapping("/kinako")
	public ModelAndView top(@RequestParam(required=false, name="startDate") String startDate,
			@RequestParam(required=false, name="endDate") String endDate, @RequestParam(required=false, name="searchCategory") String searchCategory) {

		ModelAndView mav = new ModelAndView();

		//入力された値の保持
		if(startDate != null) {
			mav.addObject("startDate", startDate);
		}
		if(endDate != null) {
			mav.addObject("endDate", endDate);
		}
		if(searchCategory != null) {
			mav.addObject("searchCategory", searchCategory);
		}

		List<Message> messageData;

		//検索フォームへの入力の有無によってDB操作を区別する
		if(startDate != null && endDate != null && searchCategory != null) {
			messageData = messageService.findByCreatedDateAndCategory(startDate, endDate, searchCategory);
		} else {
			messageData = messageService.findAllMessage();
		}

		mav.addObject("messages", messageData);

		//コメントの取得
		List<Comment> commentData = commentService.findAllComment();
		mav.addObject("comments", commentData);

		//コメント投稿用の空のオブジェクト
		Comment comment = new Comment();
		mav.addObject("commentForm", comment);

		User loginUser = (User) session.getAttribute("loginUser");
		mav.addObject("loginUser", loginUser);

		mav.setViewName("/top");

		@SuppressWarnings("unchecked")
		List<String> filterErrorMessages = (List<String>)session.getAttribute("filterErrorMessages");
		mav.addObject("filterErrorMessages", filterErrorMessages);
		session.removeAttribute("filterErrorMessages");

		return mav;

	}
}
