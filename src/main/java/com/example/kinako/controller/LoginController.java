package com.example.kinako.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.example.kinako.entity.User;
import com.example.kinako.service.UserService;
import com.example.kinako.utils.CipherUtil;

@RequestMapping("/*")
@Controller
public class LoginController {

	@Autowired
	UserService userService;

	@Autowired
	HttpSession session;

	//ログイン画面
	@GetMapping("/login")
	public ModelAndView login() {
		ModelAndView mav = new ModelAndView();

		//ログインフィルターのエラーメッセージを受け取る
		@SuppressWarnings("unchecked")
		List<String> filterErrorMessages = (List<String>)session.getAttribute("errorMessages");

		//ヘッダーの条件分岐のため、ログインしていないことを確認
		User loginUser = (User)session.getAttribute("loginUser");
		mav.addObject("loginUser", loginUser);

		mav.setViewName("/login");
		mav.addObject("filterErrorMessages", filterErrorMessages);
		return mav;
	}

	//ログイン処理
	@PostMapping("/login")
	public ModelAndView login(@ModelAttribute("account") String account,
								@ModelAttribute("password") String password) {
		List<String> errorMessages = new ArrayList<>();

		if (account.isEmpty()) {
			errorMessages.add("アカウントが入力されていません");
		}
		if (password.isEmpty()){
			errorMessages.add("パスワードが入力されていません");
		}

		//パスワードの暗号化
		String encPassword = CipherUtil.encrypt(password);
		User user = userService.findUser(account, encPassword);

		//セッションにあるエラーメッセージの削除
		session.removeAttribute("errorMessages");

		//ユーザーが見つからない、または停止中の場合
		if ((user == null || user.getIsStopped() == 1) && !account.isEmpty() && !password.isEmpty()) {
			errorMessages.add("アカウントまたはパスワードが誤っています");
		}

		if (errorMessages.size() != 0) {
			ModelAndView mav = new ModelAndView();
			mav.addObject("errorMessages", errorMessages);
			mav.addObject("account", account);
			mav.setViewName("/login");
			return mav;
		} else {
			session.setAttribute("loginUser", user);
			return new ModelAndView("redirect:/kinako");
		}
	}

	//ログアウト
	@GetMapping("/logout")
	public ModelAndView logout() {
		if(session.getAttribute("loginUser") != null) {
			session.removeAttribute("loginUser");
		}
		return new ModelAndView("redirect:/login");
	}
}
