package com.example.kinako.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.example.kinako.entity.Comment;
import com.example.kinako.entity.User;
import com.example.kinako.service.CommentService;
import com.example.kinako.service.MessageService;

@Controller
public class CommentController {

	@Autowired
	CommentService commentService;

	@Autowired
	MessageService messageService;

	@Autowired
	HttpSession session;

	@Autowired
	MessageController messageController;

	//コメント投稿処理
	@PostMapping("/addComment/{id}")
	public ModelAndView addComment(@PathVariable Integer id, @ModelAttribute("commentForm") Comment comment, RedirectAttributes redirect) {
			List<String> errorMessages = new ArrayList<>();

			if(!isValid(comment.getText(), errorMessages)) {
				redirect.addFlashAttribute("errorMessages", errorMessages);
				return new ModelAndView("redirect:/kinako");
			}

		User loginUser = (User) session.getAttribute("loginUser");
		comment.setId(0);
		comment.setUserId(loginUser.getId());
		comment.setMessageId(id);
		commentService.saveComment(comment);

		return new ModelAndView("redirect:/kinako");
	}

	//コメント削除処理
	@DeleteMapping("/deleteComment/{id}")
	public ModelAndView deleteComment(@PathVariable Integer id) {
		commentService.deleteComment(id);
		return new ModelAndView("redirect:/kinako");
	}

	private boolean isValid(String text, List<String> errorMessages) {

        if (StringUtils.isBlank(text)) {
            errorMessages.add("コメントを入力してください");
        } else if (500 < text.length()) {
            errorMessages.add("本文は500文字以下で入力してください");
        }

        if (errorMessages.size() != 0) {
            return false;
        }
        return true;
    }


}
