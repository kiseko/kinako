package com.example.kinako.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.example.kinako.entity.Branch;
import com.example.kinako.entity.Department;
import com.example.kinako.entity.User;
import com.example.kinako.model.GroupOrder;
import com.example.kinako.service.BranchService;
import com.example.kinako.service.DepartmentService;
import com.example.kinako.service.UserService;
import com.example.kinako.utils.CipherUtil;


@RequestMapping("/user/*")
@Controller
public class UserController {

	@Autowired
	UserService userService;
	@Autowired
	DepartmentService departmentService;
	@Autowired
	BranchService branchService;

	@Autowired
	HttpSession session;

	//ユーザー登録画面
	@GetMapping("/signup")
	public ModelAndView signup(@ModelAttribute("inputedUser") User inputedUser, ModelAndView mav) {

		User user = new User();
		//登録に失敗したユーザーを受け取る
		if (inputedUser != null) {
			user = inputedUser;
		}

		//支社と部署のデータを取り出す
		List<Department> departmentData = departmentService.findAllDepartment();
		List<Branch> branchData = branchService.findAllBranch();

		User loginUser = (User)session.getAttribute("loginUser");
		mav.addObject("loginUser", loginUser);

		mav.setViewName("/user/signup");
		mav.addObject("formModel", user);
		mav.addObject("departments", departmentData);
		mav.addObject("branches", branchData);
		return mav;
	}


	//ユーザー登録処理
	@PostMapping("/signup")
	public ModelAndView addUser(@ModelAttribute("formModel") @Validated(GroupOrder.class) User user, BindingResult result,
			@ModelAttribute("confirm") String confirm) {

		List<String> errorMessages = new ArrayList<>();
		List<String> passwordErrorMessages = new ArrayList<>();
		String password = user.getPassword();

		//パスワードが空の場合
		if (password.isEmpty()) {
			passwordErrorMessages.add("パスワードを入力してください");
		//パスワードのバリデーションチェック
		} else {
			isValidPassword(password, confirm, passwordErrorMessages);
		}
		//ユーザー情報のバリデーションチェック
		isValid(user, errorMessages);

		//エラーがあった場合
		if (result.hasErrors() || errorMessages.size() != 0 || passwordErrorMessages.size() != 0) {
			 ModelAndView mav = new ModelAndView();
			 mav.addObject("errorMessages", errorMessages);
			 mav.addObject("passwordErrorMessages", passwordErrorMessages);
			 return signup(user, mav);
		 }

		//パスワードの暗号化
        String encPassword = CipherUtil.encrypt(user.getPassword());
        user.setPassword(encPassword);

        //データベースへの登録
		userService.saveUser(user);
		return new ModelAndView("redirect:/user/list");
	}

	//ユーザー更新画面
	@GetMapping("/edit/{id}")
	public ModelAndView editUser(@PathVariable("id") String stringId,
			ModelAndView mav, RedirectAttributes redirectAttributes) {

		//URLのチェック
		if (!isValidParameter(stringId)) {
			String parameterError = "不正なパラメーターが入力されました";
			redirectAttributes.addFlashAttribute("parameterError", parameterError);
			return new ModelAndView("redirect:/user/list");
		}

		Integer id = Integer.parseInt(stringId);

		User user = userService.findUser(id);

		//更新に失敗したユーザーを受け取る
		User inputedUser = (User)mav.getModel().get("inputedUser");
		if (inputedUser != null) {
			user = inputedUser;
		}

		List<Department> departmentData = new ArrayList<>();
		List<Branch> branchData = new ArrayList<>();
		User loginUser = (User)session.getAttribute("loginUser");

		//総務人事部でのユーザーが自分の部署を変更できない
		if (loginUser.getDepartment().getName().equals("総務人事部")
				&& loginUser.getId() == user.getId()) {
			departmentData.add(loginUser.getDepartment());
			branchData.add(loginUser.getBranch());
		} else {
			departmentData = departmentService.findAllDepartment();
			branchData = branchService.findAllBranch();
		}

		mav.addObject("loginUser", loginUser);
		mav.setViewName("/user/edit");
		mav.addObject("formModel", user);
		mav.addObject("departments", departmentData);
		mav.addObject("branches", branchData);
		return mav;
	}

	//ユーザー更新処理
	@PutMapping("/edit/{id}")
	public ModelAndView updateUser(@PathVariable Integer id,
			@ModelAttribute("formModel") @Validated(GroupOrder.class) User user,
			BindingResult result, @ModelAttribute("confirm") String confirm,
			RedirectAttributes redirectAttributes) {

		User currentUser = userService.findUser(user.getId());
		List<String> errorMessages = new ArrayList<>();
		List<String> passwordErrorMessages = new ArrayList<>();
		String password = user.getPassword();

		isValid(user, errorMessages);

		if (!password.isEmpty()) {
			isValidPassword(password, confirm, passwordErrorMessages);
		}

		if (result.hasErrors() || errorMessages.size() != 0 || passwordErrorMessages.size() != 0) {
			String stringId = String.valueOf(id);
			ModelAndView mav = new ModelAndView();
			mav.addObject("errorMessages", errorMessages);
			mav.addObject("passwordErrorMessages", passwordErrorMessages);
			mav.addObject("inputedUser", user);
			return editUser(stringId, mav, redirectAttributes);
		 }

		user.setCreatedDate(currentUser.getCreatedDate());
		if (user.getPassword().isEmpty()) {
			user.setPassword(currentUser.getPassword());
		} else {
			String encPassword = CipherUtil.encrypt(user.getPassword());
			user.setPassword(encPassword);
		}
		userService.saveUser(user);
		return new ModelAndView("redirect:/user/list");
	}

	//ユーザー更新画面でURLにパラメーターがない場合
	@GetMapping("/edit/")
	public ModelAndView redirectList(RedirectAttributes redirectAttributes) {
		String parameterError = "不正なパラメーターが入力されました";
		redirectAttributes.addFlashAttribute("parameterError", parameterError);
		return new ModelAndView("redirect:/user/list");
	}

	//ユーザー管理画面
	@GetMapping("/list")
	public ModelAndView userList(@ModelAttribute("parameterError") String parameterError) {

		//ユーザーデータをすべて受け取る
		ModelAndView mav = new ModelAndView();
		List<User>userData = userService.findAllUser();

		User loginUser = (User)session.getAttribute("loginUser");
		mav.addObject("loginUser", loginUser);

		mav.setViewName("/user/list");
		mav.addObject("users", userData);
		//ユーザー更新画面から不正パラメーターでリダイレクトされた場合
		mav.addObject("parameterError", parameterError);
		return mav;
	}

	//ユーザー停止処理
	@PutMapping("/stop/{id}")
	public ModelAndView editUser(@PathVariable Integer id, @ModelAttribute("activeStatus") Integer activeStatus) {

		User loginUser = (User)session.getAttribute("loginUser");
		User user = userService.findUser(id);
		if (loginUser.getId() != id) {
			user.setIsStopped(activeStatus);
			userService.saveUser(user);
		}
		return new ModelAndView("redirect:/user/list");
	}

	//支社と部署の組み合わせ・既存のユーザーとのアカウント名の重複を確認
	public void isValid(User user, List<String> errorMessages) {

		Department department = departmentService.findDepartment(user.getDepartmentId());
		Branch branch = branchService.findBranch(user.getBranchId());
		User sameUser = userService.findUser(user.getAccount());

		if (branch.getName().equals("東京本社")) {
			if (department.getName().equals("営業部") || department.getName().equals("技術部")) {
				errorMessages.add("支社と部署の組み合わせが不正です");
			}
		} else {
			if (department.getName().equals("総務人事部") || department.getName().equals("情報管理部")) {
				errorMessages.add("支社と部署名の組み合わせが不正です");
			}
		}
		if (sameUser != null && sameUser.getId() != user.getId()) {
			errorMessages.add("アカウントが重複しています");
		}
	}

	//パスワードのバリデーションチェック
	public void isValidPassword (String password, String confirm, List<String> passwordErrorMessages) {

		if (!confirm.isEmpty() && !password.equals(confirm)) {
			passwordErrorMessages.add("パスワードと確認用パスワードは同じ値にしてください");
		}
		if (password.length() < 6) {
			passwordErrorMessages.add("パスワードは6文字以上で入力してください");
		} else if (password.length() > 20) {
			passwordErrorMessages.add("パスワードは20文字以下で入力してください");
		}
	}

	//ユーザー編集画面のパラメーターのバリデーションチェック
	public boolean isValidParameter(String stringId) {
		if (!stringId.matches("^[1-9]\\d*$")) {
			return false;
		}

		Integer id = Integer.parseInt(stringId);

		if (userService.findUser(id) == null) {
			return false;
		}
		return true;
	}
}
