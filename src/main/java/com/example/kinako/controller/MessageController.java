package com.example.kinako.controller;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.example.kinako.entity.Message;
import com.example.kinako.entity.User;
import com.example.kinako.service.CommentService;
import com.example.kinako.service.MessageService;

@RequestMapping("message/*")
@Controller
public class MessageController {

	@Autowired
	MessageService messageService;

	@Autowired
	CommentService commentService;

	@Autowired
	HttpSession session;

	@GetMapping("/newMessage")
	public ModelAndView newMessage() {
		ModelAndView mav = new ModelAndView();
		//投稿用の空のmessage
		Message message = new Message();
		mav.setViewName("/newMessage");
		mav.addObject("messageForm", message);
		User loginUser = (User) session.getAttribute("loginUser");
		mav.addObject("loginUser", loginUser);
		return mav;
	}

	@PostMapping("/addMessage")
	public ModelAndView addMessage(@Validated @ModelAttribute("messageForm") Message message, BindingResult result) {
		if (result.hasErrors()) {
			ModelAndView mav = new ModelAndView();
			mav.setViewName("/newMessage");
			mav.addObject("messageForm", message);
			mav.addObject("result", "エラーあり");
			User loginUser = (User) session.getAttribute("loginUser");
			mav.addObject("loginUser", loginUser);
			return mav;
		}

		User loginUser = (User) session.getAttribute("loginUser");
		message.setUserId(loginUser.getId());
		messageService.saveMessage(message);
		return new ModelAndView("redirect:/kinako");
	}

	@DeleteMapping("/deleteMessage/{id}")
	public ModelAndView deleteMessage(@PathVariable Integer id) {
		messageService.deleteMessage(id);
		return new ModelAndView("redirect:/kinako");
	}

}
