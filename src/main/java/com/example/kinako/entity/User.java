package com.example.kinako.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Length;

import com.example.kinako.model.ValidGroup1;
import com.example.kinako.model.ValidGroup2;
import com.example.kinako.model.ValidGroup3;

@Entity
@Table(name = "users")
public class User {

	@Id
	@Column
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	@Column
	@NotBlank(groups=ValidGroup1.class)
	@Size(min=6, groups=ValidGroup2.class)
	@Length(max=20, groups=ValidGroup2.class)
	@Pattern(regexp="^[^ ]+$", groups=ValidGroup3.class)
	private String account;
	@Column
	private String password;
	@Column
	@NotBlank(groups=ValidGroup1.class)
	@Size(min=1, max=10, groups=ValidGroup2.class)
	@Pattern(regexp="^[^ ]+$", groups=ValidGroup3.class)
	private String name;
	@Column(name = "branch_id")
	@NotNull
	private int branchId;
	@Column(name = "department_id")
	@NotNull
	private int departmentId;
	@Column(name = "is_stopped")
	@NotNull
	private int isStopped;
	@Column(name = "created_date")
	private Date createdDate;
	@Column(name = "updated_date")
	private Date updatedDate;

	@ManyToOne
    @JoinColumn(insertable=false, updatable=false, name="branch_id")
    private Branch branch;
	@ManyToOne
    @JoinColumn(insertable=false, updatable=false, name="department_id")
    private Department department;


	public int getId() {
		return id;
	}
	public Branch getBranch() {
		return branch;
	}
	public void setBranch(Branch branch) {
		this.branch = branch;
	}
	public Department getDepartment() {
		return department;
	}
	public void setDepartment(Department department) {
		this.department = department;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getAccount() {
		return account;
	}
	public void setAccount(String account) {
		this.account = account;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getBranchId() {
		return branchId;
	}
	public void setBranchId(int branchId) {
		this.branchId = branchId;
	}
	public int getDepartmentId() {
		return departmentId;
	}
	public void setDepartmentId(int departmentId) {
		this.departmentId = departmentId;
	}
	public int getIsStopped() {
		return isStopped;
	}
	public void setIsStopped(int isStopped) {
		this.isStopped = isStopped;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public Date getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	@PrePersist
	public void onPrePersist() {
		setCreatedDate(new Date());
		setUpdatedDate(new Date());
	}

	@PreUpdate
	public void onPreUpdate() {
		setUpdatedDate(new Date());
	}
}
