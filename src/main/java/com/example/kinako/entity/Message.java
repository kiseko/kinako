package com.example.kinako.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Entity
@Table(name = "messages")
public class Message {
	@Id
	@Column

	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@Column
	@NotBlank(message = "件名を入力してください")
	@Size(max = 30, message = "件名は30文字以下で入力してください")
	@Pattern(regexp = "^[^　]*$", message = "タイトルを入力してください")
	private String title;

	@Column
	@NotBlank(message = "本文を入力してください")
	@Size(max = 1000, message = "本文は1000文字以下で入力してください")
	@Pattern(regexp = "^[^　]*$", message = "本文を入力してください")
	private String text;

	@Column
	@NotBlank(message = "カテゴリを入力してください")
	@Size(max = 10, message = "カテゴリは10文字以下で入力してください")
	@Pattern(regexp = "^[^　]*$", message = "カテゴリを入力してください")
	private String category;

	@Column(name = "user_id")
	private int userId;

	@Column(name = "created_date")
	private Date createdDate;

	@Column(name = "updated_date")
	private Date updatedDate;

	@ManyToOne
    @JoinColumn(insertable=false, updatable=false, name="user_id")
	private User user;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}


	@PrePersist
	public void onPrePersist() {
		setCreatedDate(new Date());
		setUpdatedDate(new Date());
	}

	@PreUpdate
	public void onPreUpdate() {
		setUpdatedDate(new Date());
	}
}
