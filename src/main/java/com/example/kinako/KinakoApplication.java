package com.example.kinako;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KinakoApplication {

	public static void main(String[] args) {
		SpringApplication.run(KinakoApplication.class, args);
	}

}
