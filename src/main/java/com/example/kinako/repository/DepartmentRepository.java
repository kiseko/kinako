package com.example.kinako.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.kinako.entity.Department;

@Repository
public interface DepartmentRepository extends JpaRepository<Department, Integer> {
	List<Department> findAllByOrderById();
}
