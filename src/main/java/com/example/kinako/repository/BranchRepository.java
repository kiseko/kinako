package com.example.kinako.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.kinako.entity.Branch;


@Repository
public interface BranchRepository extends JpaRepository<Branch, Integer> {
	List<Branch> findAllByOrderById();
}
