package com.example.kinako.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.example.kinako.entity.User;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> {

	//全権取得(内部結合)
	@Query("SELECT  u FROM User u INNER JOIN u.branch INNER JOIN u.department ORDER BY u.id")
    List<User> findAll();

	User findByAccount(String account);

	//ログイン時にセッションに保存するユーザーを取得(内部結合)
	@Query("SELECT  u FROM User u INNER JOIN u.branch INNER JOIN u.department "
			+ "WHERE u.account = :account AND u.password = :password")
	User login(@Param("account") String account, @Param("password") String password);
}
