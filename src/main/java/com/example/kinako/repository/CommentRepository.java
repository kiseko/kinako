package com.example.kinako.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.kinako.entity.Comment;

@Repository
public interface CommentRepository extends JpaRepository<Comment, Integer> {

	//作成日時の降順で取得
	public List<Comment> findAllByOrderByCreatedDateAsc();
}
