package com.example.kinako.repository;

import java.sql.Timestamp;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.kinako.entity.Message;

@Repository
public interface MessageRepository extends JpaRepository<Message, Integer> {

	//作成日時の降順で取得
	public List<Message> findAllByOrderByCreatedDateDesc();

	public List<Message> findByCreatedDateBetweenAndCategoryContainingOrderByCreatedDateDesc(Timestamp start, Timestamp end, String searchCategory);
}
