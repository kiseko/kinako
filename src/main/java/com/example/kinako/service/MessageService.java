package com.example.kinako.service;

import java.sql.Timestamp;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.kinako.entity.Message;
import com.example.kinako.repository.MessageRepository;

@Service
public class MessageService {

	@Autowired
	MessageRepository messageRepository;

	//レコードの全件取得
	public List<Message> findAllMessage() {
		return messageRepository.findAllByOrderByCreatedDateDesc();

	}

	//レコードの追加
	public void saveMessage(Message message) {
		messageRepository.save(message);
	}

	//レコード1件の削除
	public void deleteMessage(Integer id) {
		Message message = messageRepository.findById(id).get();
		messageRepository.delete(message);
	}

	//検索フォームでの取得（作成日時・更新日時・カテゴリー）
	public List<Message> findByCreatedDateAndCategory(String startDate, String endDate, String searchCategory) {

		Timestamp start = changeStartDate(startDate);
		Timestamp end = changeEndDate(endDate);

	    return messageRepository.findByCreatedDateBetweenAndCategoryContainingOrderByCreatedDateDesc(start, end, searchCategory);
	}

	//絞り込みの開始日時を適切に変換する
	public Timestamp changeStartDate(String startDate) {
		Timestamp start;
	    if(!startDate.isEmpty()) {
	    	return start = Timestamp.valueOf(startDate + " 00:00:00");
	    } else {
	    	return start = Timestamp.valueOf("2020-01-01 00:00:00");
	    }
	}

	//絞り込みの終了日時を適切に変換する
	public Timestamp changeEndDate(String endDate) {
		Timestamp end;
	    if(!endDate.isEmpty()) {
	    	return end = Timestamp.valueOf(endDate + " 23:59:59");
	    } else {
	    	return end = new Timestamp(System.currentTimeMillis());
		}
	}

}
