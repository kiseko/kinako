package com.example.kinako.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.kinako.entity.Branch;
import com.example.kinako.repository.BranchRepository;

@Service
public class BranchService {

	@Autowired
	BranchRepository branchRepository;

	public List<Branch> findAllBranch() {
		return branchRepository.findAllByOrderById();
	}

	public Branch findBranch(Integer id) {
		Branch branch = branchRepository.findById(id).orElse(null);
		return branch;
	}
}
