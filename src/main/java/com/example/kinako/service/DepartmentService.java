package com.example.kinako.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.kinako.entity.Department;
import com.example.kinako.repository.DepartmentRepository;

@Service
public class DepartmentService {

	@Autowired
	DepartmentRepository departmentRepository;

	public List<Department> findAllDepartment() {
		return departmentRepository.findAllByOrderById();
	}

	public Department findDepartment(Integer id) {
		Department department = departmentRepository.findById(id).orElse(null);
		return department;
	}
}
