package com.example.kinako.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.kinako.entity.User;
import com.example.kinako.repository.UserRepository;

@Service
public class UserService {

	@Autowired
	UserRepository userRepository;

	// サインアップ
	public void saveUser(User user) {
		userRepository.save(user);
	}

	//ユーザー1件取得
	public User findUser(Integer id) {
		User user = userRepository.findById(id).orElse(null);
		return user;
	}

	public User findUser(String account) {
		User user = userRepository.findByAccount(account);
		return user;
	}

	public User findUser(String account, String password) {
		User user = userRepository.login(account, password);
		return user;
	}

	//ユーザー全件取得
	public List<User> findAllUser() {
		return userRepository.findAll();
	}
}
