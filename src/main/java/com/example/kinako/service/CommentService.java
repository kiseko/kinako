package com.example.kinako.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.kinako.entity.Comment;
import com.example.kinako.repository.CommentRepository;

@Service
public class CommentService {

	@Autowired
	CommentRepository commentRepository;

	//レコードの全件取得
	public List<Comment> findAllComment() {
		return commentRepository.findAllByOrderByCreatedDateAsc();
	}

	//レコードの追加
	public void saveComment(Comment comment) {
		commentRepository.save(comment);
	}

	//レコードの削除
	public void deleteComment(Integer id) {
		Comment comment = commentRepository.findById(id).get();
		commentRepository.delete(comment);
	}

}
