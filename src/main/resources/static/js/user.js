//停止確認ダイアログ
$(document).ready(function() {
	$(".stop-button").on("click", function() {
		if(!confirm("本当に停止しますか？")) {
			return false;
		}
	});

	$(".revival-button").on("click", function() {
		if(!confirm("本当に復活しますか？")) {
			return false;
		}
	});
});