//削除ダイアログ
$(function() {
	$('.delete-btn').on('click', function() {
		if(!confirm("本当に削除しますか？")) {
			return false;
		}
	});
});

//Topへスクロールするボタン
$(function(){
	$('#top-btn').hide();
	$(window).scroll(function(){
		if($(this).scrollTop() > 100){
			$('#top-btn').fadeIn();
		}else{
			$('#top-btn').fadeOut();
		}
	})
	$('#top-btn').on('click', function(){
		$('body, html').animate({ scrollTop: 0},
			500);
		return false;
	});
});

$(function(){
	$('#post-btn').hide();
	$(window).scroll(function(){
		if($(this).scrollTop() > 100){
			$('#post-btn').fadeIn();
		}else{
			$('#post-btn').fadeOut();
		}
	})
});